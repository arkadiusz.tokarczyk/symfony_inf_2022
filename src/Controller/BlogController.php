<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use App\Entity\BlogCategory;
use App\Form\Type\BlogCategoryType;
use App\Repository\BlogCategoryRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{

    /**
     * @Route("/", name="homepage" )
     */
    public function index(): Response
    {
        return $this->render('blog/homepage.html.twig', []);
    }

    /**
     * @Route("/articles", name="blog_list")
     */
    public function posts(BlogCategoryRepository $repository): Response
    {
        $categories = $repository->findAll();

        return $this->render('blog/list.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/categories", name="blog_categories")
     */
    public function categories(BlogCategoryRepository $repository, Request $request, ManagerRegistry $doctrine): Response
    {
        // just set up a fresh $task object (remove the example data)
        $blogCategory = new BlogCategory();

        $form = $this->createForm(BlogCategoryType::class, $blogCategory);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $blogCategory = $form->getData();
            $blogCategory->setCreatedAt(new \DateTime('now'));

            $manager = $doctrine->getManager();

            $manager->persist($blogCategory);
            $manager->flush();

            // ... perform some action, such as saving the task to the database

            return $this->redirectToRoute('blog_categories');
        }

        return $this->renderForm('blog/category_list.html.twig', [
            'categoryForm' => $form,
        ]);


    }

    /**
     * @Route("/category/{id}", name="blog_category")
     */
    public function viewCategory(string $id, BlogCategoryRepository $repository): Response
    {
        $category = $repository->find($id);
        return $this->render('blog/category.html.twig', [
            'category' => $category
        ]);
    }

    /**
     * @Route("/article/{id}", name="blog_article")
     */
    public function view(string $id): Response
    {
        return $this->render('blog/article.html.twig', []);
    }
}